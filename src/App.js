import React, { useState } from "react"; //useEffect
import "./App.css";

import SiteHeader from "./components/SiteHeader";
import VehicleForm from "./components/VehicleForm";
import VehicleList from "./components/VehicleList";
import SiteFooter from "./components/SiteFooter";

function App() {
  const [vehicleList, setVehicleList] = useState([]);

  //useEffect(() => {}, [vehicleList]);

  function addVehicle(newVehicle) {
    setVehicleList([...vehicleList, newVehicle]);
  }

  function deleteVehicle(i) {
    console.log("i:", i);
    const newList = vehicleList.filter((index) => index !== i);
    setVehicleList(newList);
    console.log(newList);
  }

  return (
    <div className="App">
      <SiteHeader />
      <VehicleForm addVehicle={addVehicle} />
      <VehicleList vehicleList={vehicleList} deleteVehicle={deleteVehicle} />
      <SiteFooter />
    </div>
  );
}

export default App;
