import React from "react";

function SiteHeader() {
  return (
    <header>
      <div
        className="container text-center"
        style={{ border: "1px solid black", margin: "auto" }}
      >
        <h1> Vehicle Database </h1>
      </div>
    </header>
  );
}

export default SiteHeader;
