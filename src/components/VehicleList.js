import React from "react";

function VehicleList(props) {
  return (
    <div className="container" style={{ border: "1px solid black" }}>
      <h5>Vehicle List </h5>

      <table className="table">
        <tbody key="tbody">
          <tr>
            <th>VIN</th>
            <th>License Plate Number</th>
            <th>Model</th>
            <th>Brand</th>
            <th>Fuel</th>
            <th>Color</th>
            <th>Vehicle Equipment</th>
            <th>Year</th>
          </tr>

          {props.vehicleList.map((vehicle, i) => (
            <tr key={i}>
              <td> {vehicle.vin} </td>
              <td> {vehicle.licensePlateNumber} </td>
              <td> {vehicle.model}</td>
              <td> {vehicle.brand}</td>
              <td> {vehicle.fuel}</td>
              <td> {vehicle.color}</td>
              <td> {vehicle.vehicleEquipment}</td>
              <td> {vehicle.year}</td>

              <td>
                <button type="button" onClick={() => props.deleteVehicle(i)}>
                  Delete vehicle
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default VehicleList;
