import React, { useState } from "react";

function VehicleForm(props) {
  const [vin, setVin] = useState("");
  const [licensePlateNumber, setLicensePlateNumber] = useState("");
  const [model, setModel] = useState("");
  const [brand, setBrand] = useState("");
  const [fuel, setFuel] = useState("");
  const [color, setColor] = useState("");
  const [vehicleEquipment, setVehicleEquipment] = useState("");
  const [year, setYear] = useState("");

  return (
    <div>
      <h3>Vehicle form</h3>

      <div className="container">
        <div className="row">
          <div className="col-12" style={{ border: "1px solid black" }}>
            <div className="form-group">
              <label htmlFor="vin"> VIN (Vehicle Identification Number)</label>
              <input
                type="text"
                id="vin"
                placeholder="Enter VIN"
                value={vin}
                onChange={(event) => {
                  setVin(event.target.value);
                }}
              />
            </div>
            <div className="form-group">
              <label htmlFor="licensePlateNumber"> License Plate Number </label>
              <input
                type="text"
                id="licensePlateNumber"
                placeholder="Enter license plate number"
                value={licensePlateNumber}
                onChange={(event) => {
                  setLicensePlateNumber(event.target.value);
                }}
              />
            </div>
            <div className="form-group">
              <label htmlFor="model"> Model</label>
              <input
                type="text"
                id="model"
                placeholder="Enter model"
                value={model}
                onChange={(event) => {
                  setModel(event.target.value);
                }}
              />
            </div>
            <div className="form-group">
              <label htmlFor="brand"> Brand</label>
              <input
                type="text"
                id="brand"
                placeholder="Enter brand"
                value={brand}
                onChange={(event) => {
                  setBrand(event.target.value);
                }}
              />
            </div>
            <div className="form-group">
              <label htmlFor="fuel"> Fuel</label>
              <input
                type="text"
                id="fuel"
                placeholder="Enter fuel"
                value={fuel}
                onChange={(event) => {
                  setFuel(event.target.value);
                }}
              />
            </div>
            <div className="form-group">
              <label htmlFor="color"> Color</label>
              <input
                type="text"
                id="color"
                placeholder="Enter color"
                value={color}
                onChange={(event) => {
                  setColor(event.target.value);
                }}
              />
            </div>
            <div className="form-group">
              <label htmlFor="vehicleEquipment"> Vehicle equipment</label>
              <input
                type="text"
                id="vehicleEquipment"
                placeholder="Enter vehicleEquipment"
                value={vehicleEquipment}
                onChange={(event) => {
                  setVehicleEquipment(event.target.value);
                }}
              />
            </div>
            <div className="form-group">
              <label htmlFor="year"> Year</label>
              <input
                type="text"
                id="year"
                placeholder="Enter year"
                value={year}
                onChange={(event) => {
                  setYear(event.target.value);
                }}
              />
            </div>
          </div>

          <button
            onClick={() => {
              const newVehicle = {
                vin,
                licensePlateNumber,
                model,
                brand,
                fuel,
                color,
                vehicleEquipment,
                year,
              };
              props.addVehicle(newVehicle);
            }}
          >
            Add vehicle
          </button>
        </div>
      </div>
    </div>
  );
}

export default VehicleForm;
