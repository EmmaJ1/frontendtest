import React from "react";

function SiteFooter() {
  return (
    <header>
      <div>
        <h6> Copyright 2021 FrontendTest </h6>
      </div>
    </header>
  );
}

export default SiteFooter;
